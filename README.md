# README #

This repository contains the files for the DM_Buffer version supporting the 14Gb optical interface on the Xilinx 
Zynq UltraScale+ MPSoC ZCU102 Evaluation Kit. The functionality of the DM_Buffer from the DMC development & X-ray 
Test System is implemented in the ODMB (as much as feasible). The ODMB implements the Optical ViewData protocol 
"downstream partner" in order to test the 16-ASIC DM design for Canon. 

### What is this repository for? ###

* Optical DM_Buffer implemenation
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* rob.crestani@redlen.com
